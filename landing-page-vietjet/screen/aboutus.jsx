import Link from 'next/link';
import { Button, Accordion, Row, Col } from "react-bootstrap";
import React, { useEffect, useState, createRef } from "react";
const View = () => {
	const [active, setActive] = useState("0")
	const onSelect = (position)=>{
		setActive(position)
	}
	return(
    <div className="aboutus">
    	<div className="gr-bg">

	    	<div className="bbalcne"/>
    	</div>
	    <div className="hd-container">
	    	<Row>
	    		<Col md={6}>
	    			<div className="content">
	    				<h3>{l.g('bhsk.landing.vj_t_7')}</h3>
	    				<Accordion defaultActiveKey={active} onSelect={onSelect}>

						  <div className={active=="0"?"abu-item-active":"abu-item"}>
						    <div>
						      <Accordion.Toggle as={Button} variant="link" eventKey="0">
								 {l.g('bhsk.landing.vj_t_8')}
						       <i className="fas fa-chevron-down"></i>
						      </Accordion.Toggle>
						    </div>
						    <Accordion.Collapse eventKey="0">
						      <div className="abu-body">
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_t_32')}</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_t_9')}</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_t_10')}</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_t_11')}</label>
								  </p>
								  {/* <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_ttk_1')}</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_ttk_2')}</label>
								  </p> */}
								 
							  </div>
						    </Accordion.Collapse>
						  </div>

						  <div className={active=="1"?"abu-item-active":"abu-item"}>
						    <div>
						      <Accordion.Toggle as={Button} variant="link" eventKey="1">
								  {l.g('bhsk.landing.vj_t_13')}
						       <i className="fas fa-chevron-down"></i>
						      </Accordion.Toggle>
						    </div>
						    <Accordion.Collapse eventKey="1">
						      <div className="abu-body">
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g('bhsk.landing.vj_cond_1')}</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 
                                        {l.g('bhsk.landing.vj_cond_2')}
                                       </label>
								    </p>
                                    {/* <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 
                                        {l.g('bhsk.landing.vj_cond_3')}
                                      </label>
								    </p>
									<p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 
                                        {l.g('bhsk.landing.vj_cond_t1')}
                                      </label>
								    </p> */}
                                    
                              </div>
						    </Accordion.Collapse>
						  </div>

						  <div className={active=="2"?"abu-item-active":"abu-item"}>
						    <div>
						      <Accordion.Toggle as={Button} variant="link" eventKey="2">
								   {l.g('bhsk.landing.vj_t_16')}
						       <i className="fas fa-chevron-down"></i>
						      </Accordion.Toggle>
						    </div>
						    <Accordion.Collapse eventKey="2">
						      <div className="abu-body">
							  		<p className="adv-item">
									  <label className="adv-item-content">{l.g('bhsk.landing.vj_dk_one')}</label>
								  	</p>
									  <p className="adv-item">
									  <label className="adv-item-content">{l.g('bhsk.landing.vj_dk_two')}</label>
								  	</p>
                                    <p className="adv-item">
									  {" "}<i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_cond_4')}</label>
								    </p>
                                    <p className="adv-item">
									{" "}<i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_cond_5')}</label>
								    </p>
                                    {/* <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_cond_6')}</label>
								    </p> */}
                                    <p className="adv-item">
									{" "}<i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_cond_7')}</label>
								    </p>
									<p className="adv-item">
									{" "}<i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> {l.g('bhsk.landing.vj_cond_8')}</label>
								    </p>
                              </div>
						    </Accordion.Collapse>
						  </div>


						  
						</Accordion>
	    			</div>
	    		</Col>
				<Col md={6}>
					<div className="aboutus-banner">
						<div className="particex"/>
						<img src="/img/vietjet/img_conditions_vj.png" className="imgbn"/>
					</div>

				</Col>
	    	</Row>
	    </div>
    </div>
)};

export default View;
