import React, { useEffect, useState, createRef } from "react";
import {Col, Row} from "react-bootstrap";
import api from "../../../../services/Network";

const ViewPackageInsur = (props) => {
    const [listPackage, setListPackage] = useState(props.listPackage)

    useEffect(() => {
        try {
            setListPackage(props.listPackage)
        } catch (e) {
            console.log(e);
        }
    }, [props.listPackage]);


    return(
        <div className="package-insurrance">
            <h1>{l.g('bhsk.landing.vj_t_17')}</h1>
            <p>{l.g('bhsk.landing.vj_t_15')}</p>
            <p>{l.g('bhsk.landing.vj_t_18')}</p>
            <div className="hd-container packge-hdbank">
                <div className="row view-positins">
                    {listPackage ? listPackage.map((data, index) =>{
                        return(
                            <div key={index}  className="col scroll-item">
                                <div className="info-item-packge">
                                    <div className="title-item-package">
                                        <h1>{data.PACK_NAME}</h1>
                                        <p>{data.FEES + ' '+l.g('bhsk.currency')}</p>
                                    </div>
                                    <div className="benefit-item-package">
                                        {data.BENEFITS ? data.BENEFITS.map((value, i) => {
                                            return(
                                                <p key={i} className="adv-item">
                                                    <i className="fas fa-check-circle"></i>
                                                    <label className="adv-item-content">{value.NAME}</label>
                                                </p>
                                            )
                                        }) : null}
                                    </div>
                                    <div className="footer-item-package">
                                        <a href={'./register-insurance.hdi?pack_code=' + data.PACK_CODE} className="btn register-item-package">{l.g('bhsk.landing.register')}</a>
                                        <p>
                                            <a href={data.URL_DETAIL} target="_blank" className="detail-item-package">{l.g('bhsk.form.lbll_detail')}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        )
                    }) : null}
                </div>
                <div>
                    <img className="img-background-package" src="/img/img_backround_package_insur.png"/>
                </div>
            </div>
        </div>
    )};

export default ViewPackageInsur;
