import Link from 'next/link';
import { Button, Accordion, Row, Col } from "react-bootstrap";
import React, { useEffect, useState, createRef } from "react";
const View = () => {
	const [active, setActive] = useState("0")
	const onSelect = (position)=>{
		setActive(position)
	}
	return(
    <div className="aboutus">
    	<div className="gr-bg">

	    	<div className="bbalcne"/>
    	</div>
	    <div className="hd-container">
	    	<Row>
	    		<Col md={6}>
	    			<div className="content">
	    				<h3>{l.g("phulong.landing.pl_t_7")}</h3>
	    				<Accordion defaultActiveKey={active} onSelect={onSelect}>

						  <div className={active=="0"?"abu-item-active":"abu-item"}>
						    <div>
						      <Accordion.Toggle as={Button} variant="link" eventKey="0">
							  {l.g("phulong.landing.pl_t_8")}
						       <i class="fas fa-chevron-down"></i>
						      </Accordion.Toggle>
						    </div>
						    <Accordion.Collapse eventKey="0">
						      <div className="abu-body">
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_9")}</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_10")}</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_11")}</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_12")}</label>
								  </p>
                                  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_13")}</label>
								  </p>
							  </div>
						    </Accordion.Collapse>
						  </div>
						  <div className={active=="1"?"abu-item-active":"abu-item"}>
						    <div>
						      <Accordion.Toggle as={Button} variant="link" eventKey="1">
							  {l.g("phulong.landing.pl_t_14")}
						       <i class="fas fa-chevron-down"></i>
						      </Accordion.Toggle>
						    </div>
						    <Accordion.Collapse eventKey="1">
						      <div className="abu-body">
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_15")}</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 
									  	{l.g("phulong.landing.pl_t_16")}
                                       </label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 
									  {l.g("phulong.landing.pl_t_17")}
                                      </label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">
                                      {l.g("phulong.landing.pl_t_18")}
                                      </label>
								    </p>
                              </div>
						    </Accordion.Collapse>
						  </div>
						  <div className={active=="2"?"abu-item-active":"abu-item"}>
						    <div>
						      <Accordion.Toggle as={Button} variant="link" eventKey="2">
							  {l.g("phulong.landing.pl_t_19")}
						       <i class="fas fa-chevron-down"></i>
						      </Accordion.Toggle>
						    </div>
						    <Accordion.Collapse eventKey="2">
						      <div className="abu-body">
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_20")}</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_21")}</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_22")}</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_23")}</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">{l.g("phulong.landing.pl_t_24")}</label>
								    </p>
                              </div>
						    </Accordion.Collapse>
						  </div>
						</Accordion>
	    			</div>
	    		</Col>
				<Col md={6}>
					<div className="aboutus-banner">
						<div className="particex"/>
						<img src="/img/bhsk/phulong/dieukienthamgia.png" className="imgbn"/>
					</div>

				</Col>
	    	</Row>
	    </div>
    </div>
)};

export default View;
