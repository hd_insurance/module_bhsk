import React, { useEffect, useState, createRef } from "react";
const ViewAdvPro = () => {
  return (
    <div className="hd-container">
      <div className="row advantages_program">
        <div className="col-md-5" style={{ textAlign: "center" }}>
          <img className="img-advant" src="/img/bhsk/hdsaison/uudiemct.png" />
        </div>
        <div className="col-md-7">
          <p className="title-advant">{l.g('saison.landing.pl_t_2')}</p>
          <div className="content-advent">
            <p className="adv-item">
              <i className="fas fa-shield-alt"></i>
              <label> {l.g('saison.landing.pl_t_3')}</label>
            </p>
            <p className="adv-item">
              <i className="fas fa-shield-alt"></i>
              <label>
                {" "}
                {l.g('saison.landing.pl_t_4')}
              </label>
            </p>
            <p className="adv-item">
              <i className="fas fa-shield-alt"></i>
              <label>{l.g('saison.landing.pl_t_5')}</label>
            </p>
            <p className="adv-item">
              <i className="fas fa-shield-alt"></i>
              <label>
              {l.g('saison.landing.pl_t_6')}
              </label>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewAdvPro;
