import React, { useEffect, useState, createRef } from "react";
import { Col, Row } from "react-bootstrap";
const ViewTutorial = () => {
  return (
    <div className="tutorial">
      <Row className="hd-container">
        <Col md={6}>
          <h1 className="title-tutorial">{l.g("bhsk.landing.vj_t_26")}</h1>
          <img
            className="img-tutorial-vietjet"
            src="/img/vietjet/img_tutorial_vj2.png"
          />
        </Col>
        <Col md={6}>
          <div className="tutorial-register">
            <div className="title-tutorial-register">
              <p>{l.g("bhsk.landing.vj_t_19")}</p>
            </div>
            <div className="content-tutorial-register">
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                  {l.g("bhsk.landing.vj_t_20")}
                </label>
              </p>
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                  {l.g("bhsk.landing.vj_t_21")}
                </label>
              </p>
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                  {l.g("bhsk.landing.vj_t_22")}
                </label>
              </p>
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                  {l.g("bhsk.landing.vj_t_23")}
                </label>
              </p>
            </div>
          </div>
          <div className="tutorial-register">
            <div className="title-tutorial-register">
              <p>{l.g("bhsk.landing.vj_t_24")}</p>
            </div>
            <div className="content-tutorial-register">
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                  {l.g("bhsk.landing.vj_t_30")}
                </label>
              </p>
              <p className="adv-item">
                {/* <i className="fas fa-check-circle"></i> */}
                <label className="adv-item-content">
                  {l.g("bhsk.landing.vj_tutorial_6")}
                  <br /> {l.g("bhsk.landing.vj_tutorial_7")} <br />
                  {l.g("bhsk.landing.vj_tutorial_8")} <br />
                  {l.g("bhsk.landing.vj_tutorial_9")}
                </label>
              </p>

              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                  {l.g("bhsk.landing.vj_t_31")}
                </label>
              </p>
              <p className="adv-item">
                {/* <i className="fas fa-check-circle"></i> */}
                <label className="adv-item-content">
                  {l.g("bhsk.landing.vj_tutorial_10")}
                  <br /> {l.g("bhsk.landing.vj_tutorial_11")} <br />
                  {l.g("bhsk.landing.vj_tutorial_12")} <br />
                  {l.g("bhsk.landing.vj_tutorial_13")}
                </label>
              </p>

              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                  {l.g("bhsk.landing.vj_t_25")}
                </label>
              </p>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default ViewTutorial;
