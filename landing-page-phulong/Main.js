import Banner from "../../../components/banner";
import React, { useEffect, useState, createRef } from "react";
import Conditions from "./screen/conditions";
import FAQ from "./../landing-page-vietjet/screen/faq";
import ViewAdvPro from './screen/advantages-program-hd';
import ViewTutorial from "./screen/tutorial-hd";
import ViewSupportCustomer from "./../landing-page-vietjet/screen/support-customer";
import ViewDocumentDown from "./../landing-page-vietjet/screen/document-dowload";
import ViewTopBanner from "./screen/top-banner-phulong";
import ViewPackageInsur from "./screen/package-insurance-hd";

import api from "../../../services/Network";



const Main = (props) => {
    
    const [listPackage, setListPackage] = useState(null)
    const [applyDate, setApplyDate] = useState("01/07/2021")

    useEffect(() => {
        try {
            getInitDataPackage()
        } catch (e) {
            console.log(e);
        }
    }, []);

    const getInitDataPackage = async () => {
        try {
            const data = await api.get("/api/bhsl/packages/PHULONG?lang="+l.getLang());
            setListPackage(data);
            if(data[0].EXP_DATE){
                setApplyDate(data[0].EXP_DATE)
            }
        } catch (e) {

        }
    };

    return (
        <div className="search-coupon-container">
            {/*top banner*/}
            <ViewTopBanner applyDate={applyDate}/>
            {/*Uu diem chuong trinh*/}
            <ViewAdvPro />
            {/*dieu kien tham gia*/}
            <Conditions />
            {/*cac goi bao hiem*/}
            <ViewPackageInsur listPackage={listPackage}/>
            {/*huong dan*/}
            <ViewTutorial />
            {/*ban danng can ho tro*/}
            <ViewSupportCustomer />
            {/*cau hoi thuong gap*/}
            <FAQ org="PHULONG"/>
            {/*tai tai lieu*/}
            <ViewDocumentDown listPackage={listPackage}/>
            {/*---------------Banner container-------*/}
            <div className="banner-app-intro">
                <div className="hd-container">
                    <Banner />
                </div>
            </div>
            {/*---------------End banner container-------*/}

        </div>
    );
};

export default Main;
