import Link from 'next/link';
import { Button, Accordion, Row, Col } from "react-bootstrap";
import React, { useEffect, useState, createRef } from "react";
const View = () => {
	const [active, setActive] = useState("0")
	const onSelect = (position)=>{
		setActive(position)
	}
	return(
    <div className="aboutus">
    	<div className="gr-bg">

	    	<div className="bbalcne"/>
    	</div>
	    <div className="hd-container">
	    	<Row>
	    		<Col md={6}>
	    			<div className="content">
	    				<h3>Điều kiện tham gia</h3>
	    				<Accordion defaultActiveKey={active} onSelect={onSelect}>

						  <div className={active=="0"?"abu-item-active":"abu-item"}>
						    <div>
						      <Accordion.Toggle as={Button} variant="link" eventKey="0">
								  Điều kiện tham gia
						       <i className="fas fa-chevron-down"></i>
						      </Accordion.Toggle>
						    </div>
						    <Accordion.Collapse eventKey="0">
						      <div className="abu-body">
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> Từ 15 ngày tuổi đến 65 tuổi</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> Không bị các bệnh tâm thần, phong</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> Không bị các bệnh ung thư</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> Nhân viên hoặc người thân của nhân viên HDBank</label>
								  </p>
								  <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> Không bị thương tật vĩnh viễn từ 70% trở lên </label>
								  </p>
                                  {/*<p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> Không đang trong thời gian điều trị bệnh/thương tật (chỉ áp dụng cho NĐBH tham gia năm đầu tiên)</label>
								  </p>*/}
							  </div>
						    </Accordion.Collapse>
						  </div>
						  <div className={active=="1"?"abu-item-active":"abu-item"}>
						    <div>
						      <Accordion.Toggle as={Button} variant="link" eventKey="1">
								  Với người thân
						       <i className="fas fa-chevron-down"></i>
						      </Accordion.Toggle>
						    </div>
						    <Accordion.Collapse eventKey="1">
						      <div className="abu-body">
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> Vợ/chồng hợp pháp</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 
                                        Con ruột, con riêng của vợ/chồng, con nuôi hợp pháp từ 15 ngày tuổi đến
                                        18 tuổi hoặc đến 24 tuổi nếu còn đang đi học, sống phụ thuộc và chưa kết hôn.
                                       </label>
								    </p>
                                    {/*<p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 
                                        Người thân chỉ được tham gia cùng thời điểm tham gia của Nhân viên
                                      </label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content">
                                      Người thân chỉ có thể đăng ký nếu Nhân viên HDBank cũng tham gia
                                      </label>
								    </p>*/}
                              </div>
						    </Accordion.Collapse>
						  </div>
						  <div className={active=="2"?"abu-item-active":"abu-item"}>
						    <div>
						      <Accordion.Toggle as={Button} variant="link" eventKey="2">
								  Thời gian chờ của nhân viên và người thân
						       <i className="fas fa-chevron-down"></i>
						      </Accordion.Toggle>
						    </div>
						    <Accordion.Collapse eventKey="2">
						      <div className="abu-body">
								  	<p className="adv-item">
									  <label className="adv-item-content">Thời gian chờ của hợp đồng này được áp dụng tính từ ngày hiệu lực bảo hiểm đầu tiên của Người được bảo hiểm và có tính liên tục khi tham gia bảo hiểm Chăm sóc sức khỏe của các năm trước.</label>
								  	</p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 30 ngày đối với điều trị bệnh thông thường</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 60 ngày đầu tiên đối với sảy thai, bỏ thai theo chỉ định của bác sĩ hoặc các biến chứng thai sản khác</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 270 ngày đầu tiên đối với sinh con</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 365 ngày đối với tử vong do bệnh đặc biệt, bệnh có sẵn, thai sản</label>
								    </p>
                                    <p className="adv-item">
									  <i className="fas fa-check-circle"></i>
									  <label className="adv-item-content"> 365 ngày đối với điều trị bệnh đặc biệt, bệnh có sẵn</label>
								    </p>
                              </div>
						    </Accordion.Collapse>
						  </div>
						</Accordion>
	    			</div>
	    		</Col>
				<Col md={6}>
					<div className="aboutus-banner">
						<div className="particex"/>
						<img src="/img/hdbank/img_conditions.png" className="imgbn"/>
					</div>

				</Col>
	    	</Row>
	    </div>
    </div>
)};

export default View;
