import React, { useEffect, useState, createRef } from "react";
import {Col, Row} from "react-bootstrap";
const ViewTutorial = () => {
    return(
        <div className="tutorial">
            <Row className="hd-container">
                <Col md={6}>
                    <h1 className="title-tutorial">Hướng dẫn</h1>
                    <img className="img-tutorial" src="/img/hdbank/img_tutorial.png" />
                </Col>
                <Col md={6}>
                    <div className="tutorial-register">
                        <div className="title-tutorial-register">
                            <p>Hướng dẫn đăng ký</p>
                        </div>
                        <div className="content-tutorial-register">
                            <p className="adv-item">
                                <i className="fas fa-check-circle"></i>
                                <label className="adv-item-content">Lựa chọn Gói bảo hiểm</label>
                            </p>
                            <p className="adv-item">
                                <i className="fas fa-check-circle"></i>
                                <label className="adv-item-content">Điền đầy đủ các thông tin vào Form đăng ký và Click “THANH TOÁN”</label>
                            </p>
                            <p className="adv-item">
                                <i className="fas fa-check-circle"></i>
                                <label className="adv-item-content">Thực hiện thanh toán bằng cách quét mã QR trên cổng thanh toán.</label>
                            </p>
                            <p className="adv-item">
                                <i className="fas fa-check-circle"></i>
                                <label className="adv-item-content">Nhận thông tin giấy chứng nhận điện tử qua email đã đăng ký khi mua.</label>
                            </p>
                        </div>
                    </div>
                    <div className="tutorial-register">
                        <div className="title-tutorial-register">
                            <p>Hướng dẫn thanh toán</p>
                        </div>
                        <div className="content-tutorial-register">
                            <p className="adv-item">
                                <i className="fas fa-check-circle"></i>
                                <label className="adv-item-content">
                                    Bước 1: Đăng nhập vào ứng dụng của ngân hàng trên điện thoại và chọn chức năng quét QR.<br/>
                                    Bước 2: Quét mã QR trên cổng thanh toán<br/>
                                    Bước 3: Kiểm tra thông tin giao dịch và thực hiện xác nhận thanh toán<br/>
                                    Bước 4: Nhập mật khẩu hoặc mã OTP để hoàn tất thanh toán<br/>
                                </label>
                            </p>
                            <p className="adv-item">
                                <i className="fas fa-check-circle"></i>
                                <label className="adv-item-content">Hợp đồng bảo hiểm không hoàn hủy</label>
                            </p>
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
    )};

export default ViewTutorial;
