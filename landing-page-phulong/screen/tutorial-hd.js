import React, { useEffect, useState, createRef } from "react";
import { Col, Row } from "react-bootstrap";
const ViewTutorial = () => {
  return (
    <div className="tutorial">
      <Row className="hd-container">
        <Col md={6}>
          <h1 className="title-tutorial">{l.g("phulong.landing.pl_tutorial")}</h1>
          <img className="img-tutorial" src="/img/bhsk/phulong/huongdan.png" />
        </Col>
        <Col md={6}>
          <div className="tutorial-register">
            <div className="title-tutorial-register">
              <p>{l.g("phulong.landing.pl_tutorial_1")}</p>
            </div>
            <div className="content-tutorial-register">
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                {l.g("phulong.landing.pl_tutorial_2")}
                </label>
              </p>
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                {l.g("phulong.landing.pl_tutorial_3")}
                </label>
              </p>
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                {l.g("phulong.landing.pl_tutorial_4")}
                </label>
              </p>
            </div>
          </div>
          <div className="tutorial-register">
            <div className="title-tutorial-register">
              <p>{l.g("phulong.landing.pl_tutorial_5")}</p>
            </div>
            <div className="content-tutorial-register">
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                {l.g("phulong.landing.pl_tutorial_6")}
                  <br />
                  {l.g("phulong.landing.pl_tutorial_7")}
                  <br />
                  {l.g("phulong.landing.pl_tutorial_8")}
                  <br />
                  {l.g("phulong.landing.pl_tutorial_9")}
                </label>
              </p>
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                {l.g("phulong.landing.pl_tutorial_10")}
                </label>
              </p>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default ViewTutorial;
