import React, { useEffect, useState, createRef } from "react";
const ViewAdvPro = () => {
    return(
        <div>
        <div className="hd-container">
            <div className="row advantages_program">
                <div className="col-md-5" style={{textAlign: 'center'}}>
                    <img className="img-advant" style={{borderRadius: '50%'}} src="/img/hdbank/img_advantages_program.png" />
                </div>
                <div className="col-md-7">
                    <p className="title-advant">Ưu điểm chương trình</p>
                    <div className="content-advent">
                        <p className="adv-item">
                            <i className="fas fa-shield-alt"></i>
                            <label> Phạm vi bảo hiểm rộng với phí bảo hiểm cạnh tranh</label>
                        </p>
                        <p className="adv-item">
                            <i className="fas fa-shield-alt"></i>
                            <label> Nhiều quyền lợi ưu đãi so với các chương trình cá nhân tự mua (phí bảo hiểm, điều khoản mở rộng,…)</label>
                        </p>
                        <p className="adv-item">
                            <i className="fas fa-shield-alt"></i>
                            <label> Được tư vấn tận tình và chu đáo về chương trình bảo hiểm từ đội ngũ HDI</label>
                        </p>
                        <p className="adv-item">
                            <i className="fas fa-shield-alt"></i>
                            <label>Được bảo lãnh viện phí toàn quốc tại các bệnh viện / phòng khám thuộc về hệ thống HDI</label>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )};

export default ViewAdvPro;
