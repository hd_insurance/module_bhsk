import Banner from "../../../components/banner";
import React, { useEffect, useState, createRef } from "react";
import AboutUs from "./screen/aboutus.jsx";
import FAQ from "./screen/faq";
import ViewAdvPro from "./screen/advantages-program";
import ViewPackageInsur from "./screen/package-insurance";
import ViewTutorial from "./screen/tutorial";
import ViewSupportCustomer from "./screen/support-customer";
import ViewDocumentDown from "./screen/document-dowload";
import ViewTopBanner from "./screen/top-banner-vietjet";

import api from "../../../services/Network";

const Main = (props) => {
    const [listPackage, setListPackage] = useState(null)
    const [applyDate, setApplyDate] = useState("31/03/2021")

    useEffect(() => {
        try {
            getInitDataPackage()
        } catch (e) {
            console.log(e);
        }
    }, []);

    const getInitDataPackage = async () => {
        try {
            const data = await api.get("/api/bhsl/packages/VIETJET_VN?lang="+l.getLang());
            setListPackage(data);
            if(data[0].EXP_DATE){
                setApplyDate(data[0].EXP_DATE)
            }
        } catch (e) {

        }
    };


  return (
    <div className="search-coupon-container">
      {/*top banner*/}
      <ViewTopBanner applyDate={applyDate}/>
       {/*Uu diem chuong trinh*/}
         <ViewAdvPro />
      {/*dieu kien tham gia*/}
        <AboutUs />
      {/*cac goi bao hiem*/}
         <ViewPackageInsur listPackage={listPackage}/>
      {/*huong dan*/}
      <ViewTutorial />
      {/*ban danng can ho tro*/}
      <ViewSupportCustomer />
      {/*cau hoi thuong gap*/}
       <FAQ org="VIETJET_VN"/>
      {/*tai tai lieu*/}
      <ViewDocumentDown listPackage={listPackage}/>

      {/*---------------Banner container-------*/}
        <div className="banner-app-intro">
          <div className="hd-container">
            <Banner />
          </div>
        </div>
      {/*---------------End banner container-------*/}

    </div>
  );
};

export default Main;
