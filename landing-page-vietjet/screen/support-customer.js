import React, { useEffect, useState, createRef } from "react";
import {Col, Row} from "react-bootstrap";
const ViewSupportCustomer = () => {
    return(
        <div className="support-kh">
            <div className="hd-container">
                <div className="background-support">
                    <h1>{l.g('bhsk.landing.vj_t_27')}</h1>
                    <p>{l.g('bhsk.landing.vj_t_28')}</p>
                    <p>{l.g('bhsk.landing.vj_t_29')}</p>
                    <Row className="contact-info">
                        <Col md={4}>
                            <p><i className="fas fa-phone-alt"></i> <a>1900 068 898</a></p>
                        </Col>
                        <Col md={4}>
                            <p><i className="fas fa-envelope-open"></i> <a>info@hdinsurance.com.vn</a></p>
                        </Col>
                        <Col md={4}>
                            <p><i className="fas fa-globe-americas"></i> <a>https://hdinsurance.com.vn</a></p>
                        </Col>
                    </Row>
                </div>
            </div>
            <div className="bg-sp">
                <img className="img-bg-sp" src="/img/img_background_partice.png"/>
            </div>
        </div>
    )};

export default ViewSupportCustomer;
