import React, { useEffect, useState, createRef } from "react";
import {Col, Row, Modal, Button} from "react-bootstrap";
import moment from 'moment'



const ViewTopBannerHD = (props) => {
  const [eventTime, setEventTime] = useState(moment(props.applyDate, "DD/MM/YYYY"))
  var currentTime = moment(new Date().getTime());
  var diffTime = eventTime - currentTime;
  const [duration, setDuration] = useState(moment.duration(diffTime, 'milliseconds'))

  useEffect(() => {
        var evtTime = moment(props.applyDate, "DD/MM/YYYY")
        setEventTime(evtTime)
        var currentTime = moment();
         console.log("EXPIRE DATE: ", props.applyDate, moment(currentTime).format("DD/MM/YYYY"))

        var diffDays = evtTime.diff(currentTime, 'days')
        var diffHours = evtTime.diff(currentTime, 'hours')
        var diffMinutes = evtTime.diff(currentTime, 'minutes')

        var d = diffDays,
                h = diffHours - (diffDays * 24),
                m = diffMinutes - (diffHours * 60);
      
      console.log(d,h,m);

        d = d.length === 1 ? '0' + d : d;
        h = h.length === 1 ? '0' + h : h;
        m = m.length === 1 ? '0' + m : m;
    
        setDuration([d,h,m])
  }, props.applyDate);

    return(
        <div className="head-top-banner">
            <div className="banner-container">
                <div className="hd-container">
                    <div className="banner">
                        <div className="child-banner-hdsaison">
                            <div className="container content-top-banner">
                                <h1>{l.g('saison.landing.pl_title_1')}</h1>
                                <h1>{l.g('saison.landing.pl_title_2')}</h1>
                                <p className="content-banner">
                                 {l.g('saison.landing.pl_t_1')}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="container form-register-bh">
                        <div className="row time-container">
                            <div className="col-12 col-md-3">
                                <p className="text-time-dk">{l.g('saison.landing.countdown_title')}</p>
                            </div>

                            <div className="col-12 col-md-6 form-time-dk">
                                   <div className="row">
                                       <div className="col-4 col-md-4 time-border-right">
                                           <p className="time-top-bannner">{duration[0]} {l.g('saison.landing.day')}</p>
                                       </div>
                                       <div className="col-4 col-md-4 time-border-right">
                                           <p className="time-top-bannner">{duration[1]} {l.g('saison.landing.hour')}</p>
                                       </div>
                                       <div className="col-4 col-md-4">
                                           {/*<label className="time-top-bannner">45 phút</>*/}
                                           <p className="time-top-bannner">{duration[2]} {l.g('saison.landing.mininute')}</p>
                                       </div>
                                   </div>
                            </div>


                            <div className="col-12 col-md-3 center-cont">
                                <a href="./register-insurance.hdi" className="btn register-bh">{l.g('saison.landing.register')}</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    )};

export default ViewTopBannerHD;
