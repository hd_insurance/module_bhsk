import React, { useEffect, useState, createRef } from "react";
import api from "../../../../services/Network";


const ViewPackgeInsurHD = (props) => {
    const [listPackage, setListPackage] = useState(props.listPackage)

    useEffect(() => {
        try {
            setListPackage(props.listPackage)
        } catch (e) {
            console.log(e);
        }
    }, [props.listPackage]);

    // const getInitDataPackage = async () => {
    //     try {
    //         console.log("X")
    //         const data = await api.get(`/api/bhsl/packages/HDSAISON?lang=vi`);
    //         setListPackage(data);
    //     } catch (e) {
    //         console.log(e);
    //     }
    // };
    return(
        <div style={{position: "relative"}}>
            <div className="hd-container packge-hdbank">
                <h1>{l.g("phulong.landing.pl_package_title")}</h1>
                <p>{l.g("phulong.landing.pl_package_title_1")}</p>
                <div className="content-packge-hdbank">
                    {listPackage ? listPackage.map((vl, i) =>(
                            <div>
                                <div className="header-content-package">
                                    <div className="name-package-insur-hdbank">
                                    {l.g("phulong.landing.pl_package_name")} {vl.PACK_NAME}
                                    </div>
                                    <div>{vl.FEES +' VNĐ/người'}</div>
                                </div>
                                <div className="benefit-package-hdbank">
                                    <div className="container">
                                        <div className="row">
                                            {vl.BENEFITS ? vl.BENEFITS.map((value, i) =>(
                                                <div className="col-md-6">
                                                    <p className="adv-item">
                                                        <i className="fas fa-check-circle"></i>
                                                        <label className="adv-item-content">{value.NAME}</label>
                                                    </p>
                                                </div>
                                            )) : null}
                                        </div>
                                    </div>
                                </div>
                                <div className="container detail-package-hdbank">
                                    <div className="row">
                                        <div className="col-12 col-md-6">
                                            <a href={'./register-insurance.hdi?pack_code='+ vl.PACK_CODE} className="btn register-package-hdbank">{l.g("phulong.landing.pl_package_dk")}</a>
                                        </div>
                                        <div className="col-12 col-md-6">
                                            <p>
                                                <a href={vl.URL_DETAIL} target="_blank" className="text-detail">{l.g("phulong.landing.pl_package_detail")}</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    )) : null}

                </div>
            </div>
            <div>
                <img className="img-background-package" src="/img/img_backround_package_insur.png"/>
            </div>
        </div>

    )};

export default ViewPackgeInsurHD;
