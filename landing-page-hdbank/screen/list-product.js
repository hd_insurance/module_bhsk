import React, { useEffect, useState, createRef } from "react";

// Import css files
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
const products = [
    {
        title: "Bảo hiểm trách nhiệm dân sự xe ô tô",
        description: "An tâm xa lộ - Bảo hộ toàn dân. Chỉ từ 480.700đ",
        link: "/page/bao-hiem-trach-nhiem-dan-su",
        benefit: "/page/bao-hiem-trach-nhiem-dan-su",
        thumb: "/img/landing/product_tnds_oto.png"
    }, 
    {
        title: "Bảo hiểm vật chất xe ô tô",
        description: "An tâm xa lộ - Bảo hộ toàn dân. Chỉ từ 480.700đ",
        link: "/product/vat-chat-xe",
        benefit: "/product/vat-chat-xe",
        thumb: "/img/landing/product_vcx.png"
    },
    {
        title: "Bảo hiểm nhà tư nhân",
        description: "Giúp khách hàng an tâm trước các rủi ro xảy ra với ngôi nhà",
        link: "/page/bao-hiem-nha-tu-nhan",
        benefit: "/page/bao-hiem-nha-tu-nhan",
        thumb: "/img/landing/product_ntn.png"
    }
]

const ProductList = () => {
  var slider = createRef()
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 1
        }
      }
    ]
  };

  return (
      <div className="products">
      <div className="pratical-01"/>
       <div className="hd-container">
       <div className="m-head-product-list">
        <h3 >Các sản phẩm khác</h3>
        <div className="sub-title">
          <p>Với mục đích mang đến trải nghiệm, quyền lợi và mức giá ưu đãi cho khách hàng,</p>
          <p>Bảo hiểm HD cung cấp đa dạng các loại bảo hiểm phù hợp với nhu cầu của khách hàng</p>
        </div>


       </div>

       <div className="group">
           <div className="list-product">
             <Slider {...settings} ref={c => (slider = c)}>
               {products.map(function(item, i){
                return <li key={i}>
                  <div className="product-item" style={{borderRadius: '12px'}}>
                    <img src={item.thumb} />
                    <div className="ifo">
                      <a href=""><h5>{item.title}</h5></a>
                      <div className="view-detail" style={{paddingBottom: '5px'}}></div>
                      {/* <a className="view-detail" href={item.benefit}>Xem quyền lợi</a> */}
                    </div>

                  </div>
                  <a href={item.link} className="kt-view-ct"><div className="view-dt">Xem chi tiết</div></a>
                </li>
              })}
               </Slider>
               


            </div>

            <div className="hd-containe xxk1">
                <div className="action-group-slide-mobile">
                  <img className="arleft" src="/img/arleft.svg" onClick={()=>slider.slickPrev()}/>
                  <img className="arright" src="/img/arright.svg" onClick={()=>slider.slickNext()}/>
                </div>
            </div>
        </div>



       </div>
      </div>
  );}
  

export default ProductList
