import React, { useEffect, useState, createRef } from "react";
import api from "../../../../services/Network";


const ViewPackgeInsurHD = (props) => {
    const [listPackage, setListPackage] = useState(props.listPackage)

    useEffect(() => {
        try {
            setListPackage(props.listPackage)
        } catch (e) {
            console.log(e);
        }
    }, [props.listPackage]);


    return (
        <div style={{ position: "relative" }}>
            <div className="hd-container packge-hdbank">
                <h1>Thông tin gói bảo hiểm</h1>
                <p>Người thân chỉ có thể đăng ký nếu Nhân viên HDBank tham gia cùng hoặc đã tham gia</p>
                <div className="content-packge-hdbank">
                    {listPackage ? listPackage.map((vl, i) => (
                        <div>
                            <div className="header-content-package">
                                <div className="name-package-insur-hdbank">
                                    Chương trình bảo hiểm sức khỏe HDB Care
                                </div>
                                <div>{vl.FEES + ' VNĐ/người'}</div>
                            </div>
                            <div className="benefit-package-hdbank">
                                <div className="container">
                                    <div className="row">
                                        {vl.BENEFITS ? vl.BENEFITS.map((value, i) => (
                                            <div className="col-md-6">
                                                <p className="adv-item">
                                                    <i className="fas fa-check-circle"></i>
                                                    <label className="adv-item-content">{value.NAME}</label>
                                                </p>
                                            </div>
                                        )) : null}
                                    </div>
                                </div>
                            </div>
                            <div className="container detail-package-hdbank">
                                <div className="row">
                                    <div className="col-12 col-md-6">
                                        <a href={'./register-insurance.hdi?pack_code=' + vl.PACK_CODE} className="btn register-package-hdbank">Đăng ký</a>
                                    </div>
                                    <div className="col-12 col-md-6">
                                        <p>
                                            <a href={vl.URL_DETAIL} target="_blank" className="text-detail">Chi tiết</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )) : null}

                </div>
            </div>
            <div>
                <img className="img-background-package" src="/img/img_backround_package_insur.png" />
            </div>
        </div>

    )
};

export default ViewPackgeInsurHD;
